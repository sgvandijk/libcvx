#ifndef __CVX_SOLVER_H__
#define __CVX_SOLVER_H__

#include <Eigen/Core>
#include "../Problem/problem.hh"

namespace cvx
{
  class Solver
  {
    public:
      virtual ~Solver () {}
  
      virtual Eigen::VectorXd solve(Problem const& problem, Eigen::VectorXd const& x0) = 0;
  };
};


#endif /* __CVX_SOLVER_H__ */

