#ifndef __CVX_PDIPSOLVER_H__
#define __CVX_PDIPSOLVER_H__

#include <Eigen/Core>
#include "../solver.hh"

namespace cvx
{
  class PDIPSolver : public Solver
  {
    public:
      PDIPSolver()
      : d_mu(10),
        d_alpha(0.005),
        d_beta(0.9),
        d_ePrim(1e-3),
        d_eDual(1e-3),
        d_eGap(1e-3)
      {}
      
      virtual Eigen::VectorXd solve(Problem const& problem, Eigen::VectorXd const& x0);
      
    private:
      double d_mu;
      double d_alpha;
      double d_beta;
      
      double d_ePrim;
      double d_eDual;
      double d_eGap;
  };
};


#endif /* __CVX_PDIPSOLVER_H__ */

