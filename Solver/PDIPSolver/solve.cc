#include "pdipsolver.ih"

VectorXd PDIPSolver::solve(Problem const& problem, VectorXd const& x0)
{
  DebugStream& dbg = DebugStream::getInstance();
  
  VectorXd lambda = 0.1 * VectorXd::Ones(problem.fi.size());
  
  VectorXd nu;
  if (problem.hi.size() > 0)
    nu = VectorXd::Zero(problem.hi.size());
  
  VectorXd x = x0;

  unsigned loops = 0;
  
  while (true)
  {
    dbg << "loop: " << ++loops << endl;
    dbg << "------------------------" << endl;
    dbg << "x:" << endl << x << endl;
    
    VectorXd fi(problem.fi.size());
    MatrixXd Dfi(fi.size(), x.size());
    for (unsigned i = 0; i < fi.size(); ++i)
    {
      fi(i) = (*problem.fi[i])(x);
      Dfi.row(i) = problem.fi[i]->grad(x).transpose();
    }
    
    dbg << "fi:" << endl << fi << endl;
    dbg << DbgHigh << "Dfi:" << endl << Dfi << endl;
    
    MatrixXd Dhi;
    if (problem.hi.size() > 0)
      Dhi = MatrixXd(problem.hi.size(), x.size());
      
    for (unsigned i = 0; i < problem.hi.size(); ++i)
    {
      Dhi.row(i) = problem.hi[i]->grad(x).transpose();
    }
    
    dbg << DbgHigh << "Dhi:" << endl << Dhi << endl;

    dbg << DbgMedium << "lambda:" << endl << lambda << endl;
    dbg << DbgMedium << "nu:" << endl << nu << endl;
    
    double gap = -fi.dot(lambda);
    
    dbg << "gap: " << gap << endl;
    
    double t = d_mu * problem.fi.size() / gap;
    dbg << "t: " << t << endl;
    
    VectorXd f0Grad = problem.f0->grad(x);
    
    dbg << DbgMedium << "f0Grad:" << endl << f0Grad << endl;
    
    VectorXd resDual = f0Grad;
    if (problem.fi.size() > 0)
      resDual += Dfi.transpose() * lambda;
    if (problem.hi.size() > 0)
      resDual += Dhi.transpose() * nu;
    VectorXd resCent = -(lambda.asDiagonal() * fi - 1.0 / t * VectorXd::Ones(lambda.size()));
    VectorXd resPrim;
    if (problem.hi.size() > 0)
      resPrim = VectorXd(problem.hi.size());
    for (unsigned i = 0; i < resPrim.size(); ++i)
      resPrim(i) = (*problem.hi[i])(x);
    
    dbg << "done residuals..." << endl;
    dbg << "resDual:" << endl << resDual << endl;
    dbg << "resCent:" << endl << resCent << endl;
    dbg << "resPrim:" << endl << resPrim << endl;
    
    if (gap < d_eGap &&
        ((resPrim.size() > 0 ? resPrim.norm() : 0) < d_ePrim) &&
        ((resDual.size() > 0 ? resDual.norm() : 0) < d_eDual))
      break;
    
    MatrixXd A(x.size() + lambda.size() + nu.size(), x.size() + lambda.size() + nu.size());
    MatrixXd h = problem.f0->hess(x);
    dbg << DbgHigh << "h0: " << endl << h << endl;
    
    for (unsigned i = 0 ; i < problem.fi.size(); ++i)
    {
      if (!problem.fi[i]->hessZero())
      {
        MatrixXd htmp = problem.fi[i]->hess(x);
        dbg << "h" << i << ": " << endl << htmp << endl;
        h += lambda[i] * problem.fi[i]->hess(x);
      }
    }
    
    ///TODO: use comma initializer?
    A.block(0, 0, x.size(), x.size()) = h;

    if (lambda.size() > 0)
      A.block(0, x.size(), x.size(), lambda.size()) =
        Dfi.transpose();

    if (nu.size() > 0)
      A.block(0, x.size() + lambda.size(), x.size(), nu.size()) =
        Dhi.transpose();
    
    
    
    if (lambda.size() > 0)
    {
      A.block(x.size(), 0, lambda.size(), x.size()) =
        -(lambda.asDiagonal() * Dfi);

      A.block(x.size(), x.size(), lambda.size(), lambda.size()) =
        -MatrixXd(fi.asDiagonal());
    }
    
    
    
    if (nu.size() > 0)
      A.block(x.size() + lambda.size(), 0, nu.size(), x.size()) = 
        Dhi;
        
    VectorXd b(resPrim.size() + resCent.size() + resDual.size());
    if (resDual.size() > 0)
      b.segment(0, resDual.size()) = resDual;
    if (resCent.size() > 0)
      b.segment(resDual.size(), resCent.size()) = resCent;
    if (resPrim.size() > 0)
      b.segment(resDual.size() + resCent.size(), resPrim.size()) = resPrim;
      
    b = -b;
    
    dbg << DbgMedium << "A:" << endl << A << endl;
    dbg << DbgMedium << "b:" << endl << b << endl;

    dbg << DbgMedium << "solving..." << endl;
    VectorXd dy = A.fullPivLu().solve(b);
    dbg << "done" << endl;
    
    dbg << "dy:" << endl << dy << endl;
    
    VectorXd dx = dy.segment(0, resDual.size());
    VectorXd dlambda;
    if (resCent.size() > 0)
      dlambda = dy.segment(resDual.size(), resCent.size());
    VectorXd dnu;
    if (resPrim.size() > 0)
      dnu = dy.segment(resDual.size() + resCent.size(), resPrim.size());

    double s = 1;
    VectorXd ll = -(lambda.array() / dlambda.array());

    dbg << "lambda: " << endl << lambda << endl;
    dbg << "dlambda: " << endl << dlambda << endl;
    dbg << "ll: " << endl << ll << endl;
    for (unsigned i = 0; i < dlambda.size(); ++i)
      if (dlambda(i) < 0 && ll(i) < s)
        s = ll(i);

    dbg << "s: " << s << endl;

    s *= 0.99;
    
    VectorXd newX = x;
    VectorXd newLambda = lambda;
    VectorXd newNu = nu;
    
    while (true)
    {
      newX = x + s * dx;
      bool valid = true;
      for (unsigned i = 0; i < fi.size(); ++i)
        if ((*problem.fi[i])(newX) >= 0)
        {
          valid = false;
          break;
        }
      if (valid)
        break;
      s *= d_beta;
    }
    
    dbg << "s: " << s << endl;

    double resLen = sqrt(resDual.squaredNorm() + 
                         (resCent.size() > 0 ? resCent.squaredNorm() : 0) + 
                         (resPrim.size() > 0 ? resPrim.squaredNorm() : 0));
    
    double oldResLen = resLen;
    
    while (true)
    {
      newX = x + s * dx;
      if (lambda.size() > 0)
        newLambda = lambda + s * dlambda;
      if (nu.size() > 0)
        newNu = nu + s * dnu;

      VectorXd newFi(problem.fi.size());
      MatrixXd newDfi(fi.size(), x.size());
      for (unsigned i = 0; i < fi.size(); ++i)
      {
        newFi(i) = (*problem.fi[i])(newX);
        newDfi.row(i) = problem.fi[i]->grad(newX).transpose();
      }
      
      MatrixXd newDhi;
      if (problem.hi.size() > 0)
        newDhi = MatrixXd(problem.hi.size(), x.size());
      for (unsigned i = 0; i < problem.hi.size(); ++i)
        newDhi.row(i) = problem.hi[i]->grad(newX).transpose();

      dbg << "newf0Grad:" << endl << problem.f0->grad(newX) << endl;
      
      VectorXd newResDual = problem.f0->grad(newX);
      if (problem.fi.size() > 0)
        newResDual += newDfi.transpose() * newLambda;
      if (problem.hi.size() > 0)
        newResDual += newDhi.transpose() * newNu;

      VectorXd newResCent(problem.fi.size());
      if (newLambda.size() > 0)
        newResCent = -(newLambda.asDiagonal() * newFi) - 1.0 / t * VectorXd::Ones(newLambda.size());
      VectorXd newResPrim;
      if (problem.hi.size() > 0)
        newResPrim = VectorXd(problem.hi.size());
      for (unsigned i = 0; i < newResPrim.size(); ++i)
        newResPrim(i) = (*problem.hi[i])(newX);

      dbg << "newResDual:" << endl << newResDual << endl;
      dbg << "newResCent:" << endl << newResCent << endl;
      dbg << "newResPrim:" << endl << newResPrim << endl;

      double newResLen = sqrt(newResDual.squaredNorm() + 
                              (newResCent.size() > 0 ? newResCent.squaredNorm() : 0) + 
                              (newResPrim.size() > 0 ? newResPrim.squaredNorm() : 0));

      dbg << "reslen: " << newResLen << " " << resLen << endl;
      
      if (newResLen < resLen && newResLen > oldResLen)
        break;
        
      oldResLen = newResLen;
      
      /*
        if (newResLen <= (1.0 - d_alpha * s) * resLen)
        break;
      */
      s *= d_beta;
      dbg << "s: " << s << endl;
    }
    
    s = s;
      
    dbg << "s: " << s << endl;

    x = newX;
    lambda = newLambda;
    nu = newNu;
  }
  
  return x;
}

