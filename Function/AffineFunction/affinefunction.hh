#ifndef __CVX_AFFINEFUNCTION_H__
#define __CVX_AFFINEFUNCTION_H__

#include "../function.hh"

namespace cvx
{
  class AffineFunction : public Function
  {
    public:
      AffineFunction (Eigen::VectorXd const& a, double const& b)
      {
        d_a = a;
        d_b = b;
      }
  
      virtual double operator()(Eigen::VectorXd const& x) const
      {
        return d_a.dot(x) + d_b;
      }
      
      virtual Eigen::VectorXd grad(Eigen::VectorXd const& x) const
      {
        return d_a;
      }
      
      virtual Eigen::MatrixXd hess(Eigen::VectorXd const& x) const
      {
        return Eigen::MatrixXd::Zero(x.size(), x.size());
      }

      virtual bool hessZero() const { return true; }
      
    private:
      Eigen::VectorXd d_a;
      double d_b;
      
  };
};


#endif /* __CVX_AFFINEFUNCTION_H__ */

