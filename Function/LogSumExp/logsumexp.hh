#ifndef __CVX_LOGSUMEXP_HH__
#define __CVX_LOGSUMEXP_HH__

#include "../function.hh"

namespace cvx
{
  class LogSumExp : public Function
  {
    public:
      LogSumExp () {}

      virtual double operator()(Eigen::VectorXd const& x) const;
      virtual Eigen::VectorXd grad(Eigen::VectorXd const& x) const;
      virtual Eigen::MatrixXd hess(Eigen::VectorXd const& x) const;
      
  };
};


#endif /* __CVX_LOGSUMEXP_HH__ */

