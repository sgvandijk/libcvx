#include "logsumexp.ih"

Eigen::MatrixXd LogSumExp::hess(Eigen::VectorXd const& x) const
{
  DebugStream& dbg = DebugStream::getInstance();

  dbg << "x: " << endl << x << endl;
  Eigen::VectorXd z = x.array().exp();
  dbg << "z: " << endl << z << endl;
  double sum = z.sum();
  dbg << "sum: " << sum << endl;
  MatrixXd h =(sum * MatrixXd(z.asDiagonal()) - z * z.transpose()) / (sum * sum);
  dbg << "h: " << h << endl; 
  return h;
}

