#include "logsumexp.ih"

double LogSumExp::operator()(Eigen::VectorXd const& x) const
{
  DebugStream& dbg = DebugStream::getInstance();

  dbg << "x: " << endl << x << endl;
  Eigen::VectorXd z = x.array().exp();
  dbg << "z: " << endl << z << endl;
  double s = z.sum();
  dbg << "sum: " << endl << s << endl;
  return log(s);
}

