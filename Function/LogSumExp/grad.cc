#include "logsumexp.ih"

Eigen::VectorXd LogSumExp::grad(Eigen::VectorXd const& x) const
{
  Eigen::VectorXd z = x.array().exp();
  return z / z.sum();
}

