#include "rddual.ih"

double RDDual::operator()(vec const& x) const
{
  vec e = d_logp + x.rows(0, x.n_elem - 2) - x(x.n_elem - 1) * d_d;
  vec z = exp(e);
  double sum = accu(z);
  return sum == 0 ? 0 : log(sum);
}

