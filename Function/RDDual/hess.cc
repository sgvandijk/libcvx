#include "rddual.ih"

mat RDDual::hess(vec const& x) const
{
  DebugStream& dbg = DebugStream::getInstance();
  dbg << DbgPush("RDDual::hess");
  
  vec e = d_logp + x.rows(0, x.n_elem - 2) - x(x.n_elem - 1) * d_d;
  vec z = exp(e);
  double sum = accu(z);
  double sum2 = sum * sum;
  
  mat h(x.n_elem, x.n_elem);
  h.submat(0, 0, e.n_elem - 1, e.n_elem - 1) = z * trans(z) / sum2 + diagmat((vec)(z / sum));
  
  double dotzd = -dot(z, d_d) ;
  vec tmp = z * dotzd / sum2 + z % -d_d / sum;
  h.submat(0, e.n_elem, e.n_elem - 1, e.n_elem) = tmp;
  h.submat(e.n_elem, 0, e.n_elem, e.n_elem - 1) = trans(tmp);
  h(e.n_elem, e.n_elem) = dotzd * dotzd / sum2 + dot(z, d_d % d_d) / sum;
  
  return h;
}

