#include "rddual.ih"

vec RDDual::grad(vec const& x) const
{
  DebugStream& dbg = DebugStream::getInstance();
  //dbg << DbgPush("RDDual::grad");
  
  vec e = d_logp + x.rows(0, x.n_elem - 2) - x(x.n_elem - 1) * d_d;
  
  //dbg << DbgTrace << "e:" << endl << e << endl;

  vec z = exp(e);
  //dbg << DbgTrace << "z:" << endl << z << endl;

  double sum = accu(z);
  //dbg << DbgTrace << "sum:" << sum << endl;

  vec res(x.n_elem);
  res.rows(0, z.n_elem - 1) = z;
  res(z.n_elem) = dot(z, -d_d);
  
  if (sum !=0)
    res /= sum;
  
  //dbg << DbgPop;
  
  return res;
}

