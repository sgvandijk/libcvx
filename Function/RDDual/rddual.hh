#ifndef __CVX_RDDUAL_HH__
#define __CVX_RDDUAL_HH__

#include "../function.hh"

namespace cvx
{
  class RDDual : public Function
  {
    public:
      RDDual (arma::vec const& p, arma::vec const& d)
      : d_logp(arma::log(p)),
        d_d(d)
      {}
  
      virtual double operator()(arma::vec const& x) const;
      virtual arma::vec grad(arma::vec const& x) const;
      virtual arma::mat hess(arma::vec const& x) const;

    private:
      arma::vec d_logp;
      arma::vec d_d;
  };
};


#endif /* __CVX_RDDUAL_HH__ */

