#ifndef __CVX_DISTORTION_HH__
#define __CVX_DISTORTION_HH__

#include "../function.hh"

namespace cvx
{
  class Distortion : public Function
  {
    public:
      Distortion (Eigen::RowVectorXd const& p_x, Eigen::MatrixXd const rho, double D)
      : d_p_x(p_x),
        d_rho(rho),
        d_D(D)
      {}
      
      virtual double operator()(Eigen::VectorXd const& x) const;
      
      virtual Eigen::VectorXd grad(Eigen::VectorXd const& x) const;
      
      virtual Eigen::MatrixXd hess(Eigen::VectorXd const& x) const;
      
      virtual bool hessZero() const { return true; }

    private:
      Eigen::VectorXd d_p_x;
      Eigen::MatrixXd d_rho;
      double d_D;
  };
};


#endif /* __CVX_DISTORTION_HH__ */

