#include "distortion.ih"

double Distortion::operator()(VectorXd const& x) const
{
  unsigned nX = d_p_x.size();
  unsigned nY = x.size() / nX;
  
  mat p_y_x = x;
  p_y_x.resize(nX, nY);
  
  return (d_p_x.transpose().dot(((p_y_x.array() * d_rho).rowwise().sum()))(0) - d_D;
  //return conv_to<double>::from(sum(trans(d_p_x) % sum(p_y_x % d_rho, 1))) - d_D;
}

