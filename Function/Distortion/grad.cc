#include "distortion.ih"

Eigen::VectorXd Distortion::grad(Eigen::VectorXd const& x) const
{
  unsigned nX = d_p_x.size();
  unsigned nY = x.size() / nX;
  
  Eigen::MatrixXd p_y_x =  x;
  p_y_x.resize(nX, nY);
  Eigen::MatrixXd g(nX, nY);
  
  for (unsigned x = 0; x < nX; ++x)
    for (unsigned y = 0; y < nY; ++y)
      g(x, y) = d_p_x(x) * d_rho(x, y);
  
  g.resize(x.size(), 1);
  return g;
}

