#include "mutualinformation2.ih"

mat MutualInformation2::hess(vec const& x) const
{
  unsigned nX = d_p_x.n_elem;
  unsigned nY = x.n_elem / nX;

  mat p_y_x = reshape(x, nX, nY);
  rowvec p_y = d_p_x * p_y_x;

  mat h(x.n_elem, x.n_elem);
  for (unsigned i = 0; i < h.n_rows; ++i)
    for (unsigned j = 0; j < h.n_cols; ++j)
    {
      unsigned xi = i % nX;
      unsigned yi = i / nX;
      unsigned xj = j % nX;
      unsigned yj = j / nX;
      h(i, j) = (i == j ? d_p_x(xi) / p_y_x(xi, yi) : 0) +
                (yi == yj ? -d_p_x(xi) / p_y(yi) * d_p_x(xj) : 0);
    }
  
  return h;
}

