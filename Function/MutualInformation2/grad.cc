#include "mutualinformation2.ih"

vec MutualInformation2::grad(vec const& x) const
{
  unsigned nX = d_p_x.n_elem;
  unsigned nY = x.n_elem / nX;
  
  mat p_y_x = reshape(x, nX, nY);
  rowvec p_y = d_p_x * p_y_x;
  
  mat g(nX, nY);
  for (unsigned x = 0; x < nX; ++x)
    for (unsigned y = 0; y < nY; ++y)
      g(x,y) = p_y(y) == 0 ? 0 : d_p_x(x) * IT::log(p_y_x(x, y) / p_y(y));
      
  return reshape(g, x.n_elem, 1);
}

