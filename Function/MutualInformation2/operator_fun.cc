#include "mutualinformation2.ih"

double MutualInformation2::operator()(vec const& x) const
{
  unsigned nX = d_p_x.n_elem;
  unsigned nY = x.n_elem / nX;
  
  mat p_y_x = reshape(x, nX, nY);
  rowvec p_y = d_p_x * p_y_x;
  
  double mi = 0;
  
  for (unsigned x = 0; x < nX; ++x)
  {
    double i = 0;
    for (unsigned y = 0; y < nY; ++y)
      i += p_y(y) == 0 ? 0 : IT::log(p_y_x(x, y) / p_y(y));
    mi += d_p_x(x) * i;
  }
  
  return mi;
}

