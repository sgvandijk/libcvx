#ifndef __CVX_MUTUALINFORMATION1_HH__
#define __CVX_MUTUALINFORMATION1_HH__

#include "../function.hh"

namespace cvx
{
  class MutualInformation2 : public Function
  {
    public:
      MutualInformation2 (arma::rowvec const& p_x)
      : d_p_x(p_x)
      {}
      
      virtual double operator()(arma::vec const& x) const;
      
      virtual arma::vec grad(arma::vec const& x) const;
      
      virtual arma::mat hess(arma::vec const& x) const;
      
    private:
      arma::rowvec d_p_x;
  };
};


#endif /* __CVX_MUTUALINFORMATION1_HH__ */

