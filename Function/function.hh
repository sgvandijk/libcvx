#ifndef __CVX_FUNCTION_H__
#define __CVX_FUNCTION_H__

#include <Eigen/Core>

namespace cvx
{
  class Function
  {
    public:
      Function () {}
      virtual ~Function () {}
  
      virtual double operator()(Eigen::VectorXd const& x) const = 0;
      
      virtual Eigen::VectorXd grad(Eigen::VectorXd const& x) const = 0;
      
      virtual Eigen::MatrixXd hess(Eigen::VectorXd const& x) const = 0;
      
      virtual bool hessZero() const { return false; }
    private:
      
  };
};


#endif /* __CVX_FUNCTION_H__ */

