#ifndef __CVX_PROBLEM_H__
#define __CVX_PROBLEM_H__

#include "../Function/function.hh"
#include <vector>

namespace cvx
{
  struct Problem
  {
    public:
      Problem () {}
    
      /// Objective function
      Function* f0;
      
      /// Inequality constraint functions
      std::vector<Function*> fi;
      
      /// Equality constraint functions
      std::vector<Function*> hi;
  };
};


#endif /* __CVX_PROBLEM_H__ */

